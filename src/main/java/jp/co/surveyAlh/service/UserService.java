package jp.co.surveyAlh.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.surveyAlh.dto.UserDto;
import jp.co.surveyAlh.entity.UserEntity;
import jp.co.surveyAlh.mapper.UserMapper;
import jp.co.surveyAlh.utils.CipherUtil;

@Service
public class UserService {

	@Autowired
	private UserMapper userMapper;

	/**
	 * @param 保存するユーザー情報
	 * @author ikegami.masaki
	 *
	 * 新規でユーザーを保存する
	 * */
	public int insert(UserDto userDto) {

		// Entityにコピー
		UserEntity user = new UserEntity();
		BeanUtils.copyProperties(userDto, user );

		// パスワードをハッシュ化
		String password = CipherUtil.encrypt(user.getPassword());
		user.setPassword(password);

		int count = userMapper.insert(user);
		return count;
	}

	/**
	 * @param loginId ログインID
	 * @param password パスワード
	 * @author ikegami.masaki
	 *
	 * @return ログインIDとパスワードを元にユーザーを取得
	 * */
	public UserDto getUser(String loginId, String password) {

		password = CipherUtil.encrypt(password);

		UserEntity user = userMapper.getUser(loginId, password);
		UserDto userDto = new UserDto();
		if (user != null) {

			BeanUtils.copyProperties(user, userDto);
			return userDto;
		} else {

			return null;
		}
	}

	/**
	 * @author ikegami.masaki
	 *
	 * @return データベース上のすべてのユーザー
	 * 最新の回答の結果の情報をもっている
	 * */
	public List<UserDto> getUsers() {

		List<UserEntity> usersEntity = userMapper.getUsers();
		List<UserDto> ret = convertToList(usersEntity);

		return ret;
	}

	// --------------------------------
	// privateメソッド

	/**
	 * @author ikegami.masaki
	 * @param DBで検索した結果のList<Entity>
	 *
	 * @return 検索した値を移し変えた　List<UserDto>
	 * */
	private List<UserDto> convertToList(List<UserEntity> entity) {
		List<UserDto> ret = new LinkedList<UserDto>();
		for (UserEntity user : entity) {
			UserDto dto = new UserDto();
			BeanUtils.copyProperties(user, dto);
			ret.add(dto);
		}
		return ret;
	}

}
