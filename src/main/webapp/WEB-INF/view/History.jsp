<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>履歴</title>
</head>
<body>
	<%@ include file="Header.jsp" %>
	<h2>履歴</h2>
	<table>
		<tr>
			<th>回答日時</th>
			<th>結果</th>
		</tr>

		<c:forEach items="${results}" var="result">
			<tr>
				<td><fmt:formatDate value="${result.createdDate}"
						pattern="yyyy/MM/dd" /></td>
			</tr>
		</c:forEach>
	</table>

	<canvas id="myChart"></canvas>

	<c:forEach items="${results}" var="result">
		<div id="driveNumber" hidden>
			<c:out value="${result.driveNumber}" />
		</div>
		<div id="volunteerNumber" hidden>
			<c:out value="${result.volunteerNumber}" />
		</div>
		<div id="analyzeNumber" hidden>
			<c:out value="${result.analyzeNumber}" />
		</div>
		<div id="createNumber" hidden>
			<c:out value="${result.createNumber}" />
		</div>
	</c:forEach>
</body>
<script>
	var ctx = document.getElementById("myChart");
	var drive_number = document.getElementById("driveNumber").textContent;
	var volunteer_number = document.getElementById("volunteerNumber").textContent;
	var analyze_number = document.getElementById("analyzeNumber").textContent;
	var create_number = document.getElementById("createNumber").textContent;
	var myChart = new Chart(ctx, {
		type : 'radar',
		data : {
			labels : [ "ドライブ", "クリエイト", "ボランティア", "アナライズ" ],
			datasets : [ {
				label : 'apples',
				backgroundColor : "rgba(153,255,51,0.4)",
				borderColor : "rgba(153,255,51,1)",
				data : [ drive_number, create_number, volunteer_number,
						analyze_number ]
			} ]
		},
		options : {
			scale : {
				ticks : {
					max : 20,
					min : 0
				}
			}
		}
	});
</script>
</html>