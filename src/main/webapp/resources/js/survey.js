var num = 0;
var speed = 300;  //スライドの速度
var nextQ = num;

function nq(){
	  num ++;
	  nextQ = num;
	}

function bq(){
	  num --;
	  nextQ = num;
	}

$(function() {
	if(num == 0){
		$('#back').hide()
	}
	$('#next').on('click', function() {
		 $('#'+nextQ).animate({ marginLeft: -650}, speed);
		 nq();
		 $('#'+nextQ).animate({ marginLeft: 0}, speed);
		 $('#back').show()
		 if(num == 9){
				$('#next').hide()
			}
	});

	$('#back').on('click', function() {
		$('#'+nextQ).animate({ marginLeft: 650}, speed);
		bq();
		$('#'+nextQ).animate({ marginLeft: 0}, speed);
		if(num == 0){
			$('#back').hide()
		}
		if(num != 9){
			$('#next').show()
		}
	});
});
