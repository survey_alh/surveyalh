<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<%@ include file="Header.jsp" %>

	<form action="survey">
		<select name="id">
			<c:forEach items="${surveys }" var="survey">
				<option value="<c:out value="${survey.id }"/>"><c:out value="${survey.surveyTitle}"/></option]>
			</c:forEach>
		</select>

		<input type="submit" />
	</form>

	<!-- 最新の診断結果をチャートで表示 -->
	<!-- 以下テスト表示 -->
	<c:out value="${newResult.driveNumber }" />
	<c:out value="${newResult.volunteerNumber }" />
	<c:out value="${newResult.analyzeNumber }" />
	<c:out value="${newResult.createNumber }" />
	<c:out value="${newResult.personalityType }" />
	<c:out value="${newResult.thinkingType }" />
</body>
</html>