<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8>
<title>アンケート</title>
<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
<link href="<c:url value="/resources/css/survey.css" />" rel="stylesheet">
<script src="<c:url value="/resources/js/survey.js" />"></script>

</head>
<body>
	<div class ="surveyTitle"><c:out value="${surveys[1].surveyTitle }" /></div>
	<div id="survey">
		<form:form method = "POST" modelAttribute="apForm">
			<c:forEach items="${apForm.getAnswerForm() }" varStatus="status">
				<div class="questions" id="${status.index }">
					<div class="questionTitle">
						<c:out value="${surveys[status.index].questionNumber}"/>
						<c:out value="${surveys[status.index].title}"/>
					</div>
					<div class="choiceA">
						<c:out value="${surveys[status.index].choiceA }"/>
						<form:radiobutton  label="◎" value="2" path="answerForm[${status.index}].driveNumber" />
						<form:radiobutton  label="○" value="1" path="answerForm[${status.index}].driveNumber" />
						<form:radiobutton  label="×" value="0" path="answerForm[${status.index}].driveNumber" checked="checked"/>
					</div>
					<div class="choiceB">
						<c:out value="${surveys[status.index].choiceB }"/>
						<form:radiobutton  label="◎" value="2" path="answerForm[${status.index}].analyzeNumber" />
						<form:radiobutton  label="○" value="1" path="answerForm[${status.index}].analyzeNumber" />
						<form:radiobutton  label="×" value="0" path="answerForm[${status.index}].analyzeNumber" checked="checked"/>
					</div>
					<div class="choiceC">
						<c:out value="${surveys[status.index].choiceC }"/>
						<form:radiobutton  label="◎" value="2" path="answerForm[${status.index}].createNumber" />
						<form:radiobutton  label="○" value="1" path="answerForm[${status.index}].createNumber" />
						<form:radiobutton  label="×" value="0" path="answerForm[${status.index}].createNumber" checked="checked"/>
					</div>
					<div class="choiceD">
						<c:out value="${surveys[status.index].choiceD }"/>
						<form:radiobutton  label="◎" value="2" path="answerForm[${status.index}].volunteerNumber" />
						<form:radiobutton  label="○" value="1" path="answerForm[${status.index}].volunteerNumber" />
						<form:radiobutton  label="×" value="0" path="answerForm[${status.index}].volunteerNumber" checked="checked"/>
					</div>
					<form:hidden path="answerForm[${status.index}].userId" value="${loginUser.id }"/>
					<form:hidden path="answerForm[${status.index}].questionId" value="${surveys[status.index].questionId}"/>
					<form:hidden path="answerForm[${status.index}].questionId" value="${surveys[status.index].questionId}"/>
					<c:if test="${status.index == 9 }">
						<input type="submit" value="アンケートに回答"/>
					</c:if>
				</div>
			</c:forEach>
		</form:form>
		<div id="next">next</div>
		<div id="back">back</div>
	</div>
</body>
</html>